<?php
declare(strict_types=1);

namespace Beside\Shopfinder\Api\Data;

use Redbox\Shopfinder\Api\Data\ShopInterface as RedboxShopInterface;

/**
 * Interface ShopInterface
 *
 * @package Beside\Shopfinder\Api\Data
 */
interface ShopInterface extends RedboxShopInterface
{
    /** @var string shop code column name */
    const SHOP_CODE = 'shop_code';

    /**
     * Get shop code
     *
     * @return string
     */
    public function getShopCode(): string;

    /**
     * Set shop code
     *
     * @param string $shopCode
     *
     * @return ShopInterface
     */
    public function setShopCode(string $shopCode): self;
}
