<?php
declare(strict_types=1);

namespace Beside\Shopfinder\Console\Command;

use Beside\Shopfinder\Model\ShopsImport;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Exception\FileSystemException;

/**
 * Class ImportShopfinderShops
 *
 * @package Beside\Shopfinder\Console\Command
 */
class ImportShopfinderShops extends Command
{
    /**
     * Path to csv file in var directory
     */
    const FILE_NAME = 'file-name';

    /**
     * @var ShopsImport
     */
    private ShopsImport $shopsImport;

    /**
     * ImportShopfinderShops constructor.
     *
     * @param ShopsImport $shopsImport
     */
    public function __construct(
        ShopsImport $shopsImport
    ) {
        parent::__construct();
        $this->shopsImport = $shopsImport;
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $options = [
            new InputArgument(
                self::FILE_NAME,
                InputArgument::REQUIRED,
                'File name'
            )
        ];

        $this->setName('beside:import:shops')
            ->setDescription('Import shops data from CSV file to Shopfinder table')
            ->setDefinition($options);

        parent::configure();
    }

    /**
     * Execute method for import shops
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return $this|Command
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): Command
    {
        $fileName = $input->getArgument(self::FILE_NAME);
        try {
            $result = $this->shopsImport->importShops($fileName);
            $output->writeln($result . ' entries has been imported');
        } catch (FileSystemException $e) {
            $output->writeln($e);
        }

        return $this;
    }
}
