<?php

namespace Beside\Shopfinder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Beside\Erp\Api\Data\ErpRequestInterface;
use Magento\Framework\App\Helper\Context;
use Redbox\Shopfinder\Api\ShopRepositoryInterface;
use Redbox\Shopfinder\Model\ResourceModel\Shop;
use Magento\Framework\Model\AbstractModelFactory;

class Data extends AbstractHelper
{
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * @var Shop
     */
    private $resourceModelFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param ShopRepositoryInterface $shopRepository
     * @param Shop $resourceModel
     * @param AbstractModelFactory $abstractModelFactory
     */
    public function __construct(
        Context $context,
        ShopRepositoryInterface $shopRepository,
        Shop $resourceModel,
        AbstractModelFactory $abstractModelFactory
    ) {
        $this->shopRepository = $shopRepository;
        $this->resourceModel = $resourceModel;
        $this->abstractModelFactory = $abstractModelFactory;
        parent::__construct($context);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    public function getShopData(\Magento\Sales\Model\Order $order) {
        $result = [];

        if ($this->isStorePickUp($order->getShippingMethod()) && $order->getBillingAddress()->getPickupLocationId()) {
            $pickupLocationId = $order->getBillingAddress()->getPickupLocationId();

            try {
                $connection = $this->resourceModel->getConnection();

                $select = $connection->select()->from(
                    $this->resourceModel->getTable('shopfinder_shop')
                )->where(
                    \Redbox\Shopfinder\Api\Data\ShopInterface::IDENTIFIER . ' = :' . \Redbox\Shopfinder\Api\Data\ShopInterface::IDENTIFIER
                );

                $binds = [':' . \Redbox\Shopfinder\Api\Data\ShopInterface::IDENTIFIER => $pickupLocationId];
                $result = $connection->fetchRow($select, $binds);
            } catch (\Exception $exception) {
                //Can be used for debugging.
            }
        }

        return $result;
    }

    /**
     * @param $shippingtCode
     * @return bool
     */
    private function isStorePickUp($shippingtCode)
    {
        return $shippingtCode === 'beside_store_pickup_beside_store_pickup';
    }
}
