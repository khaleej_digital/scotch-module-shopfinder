<?php
declare(strict_types=1);

namespace Beside\Shopfinder\Model;

use Beside\Shopfinder\Api\Data\ShopInterface;
use Redbox\Shopfinder\Model\Shop as RedboxShop;

/**
 * Class Shop
 *
 * @package Beside\Shopfinder\Model
 */
class Shop extends RedboxShop implements ShopInterface
{
    /**
     * Get shop code
     *
     * @return string
     */
    public function getShopCode(): string
    {
        return (string) $this->getData(self::SHOP_CODE);
    }

    /**
     * Set shop code
     *
     * @param string $shopCode
     *
     * @return ShopInterface
     */
    public function setShopCode(string $shopCode): ShopInterface
    {
        return $this->setData(self::SHOP_CODE, $shopCode);
    }
}
