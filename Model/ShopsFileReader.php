<?php
declare(strict_types=1);

namespace Beside\Shopfinder\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Phrase;

/**
 * Class ShopsFileReader
 *
 * @package Beside\Shopfinder\Model
 */
class ShopsFileReader
{
    /**
     * Import dir name (in /var directory)
     */
    public const IMPORT_DIR =  'import' . DIRECTORY_SEPARATOR . 'shops';

    /**
     * @var DirectoryList
     */
    private DirectoryList $directoryList;

    /**
     * @var Csv
     */
    private Csv $csv;

    /**
     * @var File
     */
    private File $fileDriver;

    /**
     * ShopsFileReader constructor.
     *
     * @param DirectoryList $directoryList
     * @param File $fileDriver
     * @param Csv $csv
     */
    public function __construct(
        DirectoryList $directoryList,
        File $fileDriver,
        Csv $csv
    ) {

        $this->directoryList = $directoryList;
        $this->csv = $csv;
        $this->fileDriver = $fileDriver;
    }

    /**
     * Get file content if file exist
     *
     * @param string $fileName
     *
     * @throws FileSystemException
     * @return mixed
     */
    public function getFile(string $fileName)
    {
        $filePath = $this->directoryList->getPath(DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR .
            self::IMPORT_DIR . DIRECTORY_SEPARATOR . $fileName;
        $file = $this->fileDriver->isExists($filePath);
        if (!$file) {
            throw new FileSystemException(__('File not found'));
        }
        $csvData = $this->csv->getData($filePath);

        return $csvData;
    }
}
