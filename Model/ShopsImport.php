<?php
declare(strict_types=1);

namespace Beside\Shopfinder\Model;

use Beside\Shopfinder\Model\ShopFactory;
use Exception;
use Magento\Directory\Model\CountryFactory;
use Redbox\Shopfinder\Api\ShopRepositoryInterface;
use Redbox\Shopfinder\Api\Data\ShopInterface;
use Redbox\Shopfinder\Model\ResourceModel\Shop\CollectionFactory as ShopCollectionFactory;

/**
 * Class ShopsImport
 *
 * @package Beside\Shopfinder\Model
 */
class ShopsImport
{
    /**
     * @var ShopFactory
     */
    private ShopFactory $shopFactory;

    /**
     * @var ShopRepositoryInterface
     */
    private ShopRepositoryInterface $shopRepository;

    /**
     * @var CountryFactory
     */
    private CountryFactory $countryFactory;

    /**
     * @var ShopsFileReader
     */
    private ShopsFileReader $fileReader;

    /**
     * @var ShopCollectionFactory
     */
    private ShopCollectionFactory $shopCollectionFactory;

    /**
     * ShopsImport constructor.
     *
     * @param ShopFactory $shopFactory
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopsFileReader $fileReader
     * @param ShopCollectionFactory $shopCollectionFactory
     * @param CountryFactory $countryFactory
     */
    public function __construct(
        ShopFactory $shopFactory,
        ShopRepositoryInterface $shopRepository,
        ShopsFileReader $fileReader,
        ShopCollectionFactory $shopCollectionFactory,
        CountryFactory $countryFactory
    ) {
        $this->shopFactory = $shopFactory;
        $this->shopRepository = $shopRepository;
        $this->countryFactory = $countryFactory;
        $this->fileReader = $fileReader;
        $this->shopCollectionFactory = $shopCollectionFactory;
    }

    /**
     * Import shops from CSV file data to Shopfinder table
     *
     * @param string $fileName
     *
     * @return int
     * @throws Exception
     */
    public function importShops(string $fileName): int
    {
        $shopsInfo = $this->fileReader->getFile($fileName);
        $i = 0;
        array_shift($shopsInfo);
        if ($shopsInfo) {
            foreach ($shopsInfo as $shopData) {
                $this->createShop($shopData);
                $i++;
            }
        }

        return $i;
    }

    /**
     * Create Shopfinder shop
     *
     * @param array $shopData
     *
     * @throws Exception
     */
    private function createShop(array $shopData): void
    {
        /** @var \Beside\Shopfinder\Model\Shop $shop */
        $identifier = $shopData[0] ?? '';
        if ($identifier) {
            $shop = $this->getShop($identifier);
            $shop->setIdentifier($identifier);
            $shop->setShopCode($identifier);
            $shop->setShopName($shopData[1] ?? '');
            $shop->setStreet($shopData[2] ?? '');
            $shop->setCity($shopData[3] ?? '');
            $shop->setCountryId($this->getCountryId($shopData[4] ?? ''));
            $shop->setTelephone($shopData[5] ?? '');
            $shop->getShopEmail($shopData[6] ?? '');
            $shop->save();
        }
    }

    /**
     * Get county ID by country code
     *
     * @param string $countryCode
     *
     * @return string
     */
    private function getCountryId(string $countryCode): string
    {
        $countryId = '';
        switch ($countryCode) {
            case 'KSA':
                $countryId = 'SA';
                break;
            case 'UAE':
                $countryId = 'AE';
                break;
            default:
                $country = $this->countryFactory->create()->loadByCode($countryCode);
                if ($country) {
                    $countryId = $country->getId();
                }
        }

        return $countryId;
    }

    /**
     * Get shop model object
     *
     * @param string $shopId
     *
     * @return ShopInterface
     */
    private function getShop( string $shopId): ShopInterface
    {
        /** @var  $collection \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection */
        $collection = $this->shopCollectionFactory->create();
        // collection is used instead of repository to avoid problems with setting/getting region
        $collection->addFieldToFilter('identifier', $shopId);
        if ($collection->getSize()) {
            $shop = $collection->getFirstItem();
        } else {
            $shop = $shop = $this->shopFactory->create();
            $shop->setStatus(1);
            $shop->setCanCollect(1);
        }

        return $shop;
    }
}
